
-- Card definition:
--
-- Each card is defined by a table with at least one element
-- The element is the text, it can be:
-- a) a string with a single line
-- b) a single string with newlines
-- c) a table of strings with newlines (each one will be another line)
-- d) a function (will be called at card creation, has to return a string or table from the former formats
--
-- ctype: the type of card
-- i) "skip" (the default): creates two buttons, "next" and "skip"
-- ii) "next": one single button, "next"
-- iii) "question": creates 2 cards, the first shows the question number,
--       the second shows the question text and a "done" button
-- iv) "answer": creates 2 cards: the first is the answer with 2 buttons "yes" or "no"
--       the second shows the current score (updated if necessary)
-- v) "jump": a skip card without "next"
-- vi) "special": the second element is a table defining one or two buttons
--       each entry is a table with "blabel", "callback" and or "ctype"
--		 where "ctype" can be "next", "jump", "skip" or "exit" for predefined callbacks
--		 if no ctype is given or it's unrecognized, a callback must be provided
--
-- how skip cards work: if the user presses "skip", the game will jump to the next card with a label
-- unless a skiplabel is specified
--
-- label: a string to identify the card, it's a landing position for "skip" cards
-- skiplabel: destination of a skip or a jump if it's not going to be the next one
-- 
-- blabel: button label, overriding the default (depends on type of card), for single-button cards
-- blabels: a table with two strings, one for each button to override
--
-- pre: a function to be executed when the card is instantiated
--
--

-- example:
--	Flow = {
--	{ "intro", ctype="next" },
--	{ "switch full screen?", ctype="special",
--		{ { blabel="yes", ctype="yes", callback=toggleFullscreen },
--		  { blabel="no", ctype="next" } } },
--    { "this is a game" },
--	{ "that you can win" },
--	{ label="start", "start game", ctype="next", blabel="start",
--	  pre = function() score = 0 end },
--	{ ctype="question", "what is the noise of an elephant?" },
--	{ ctype="answer", "pweeeet" },
--	{ "that was the last question", ctype="next" },
--	{ ctype = "next",
--	  function() 
--		local txt = "Final score:\n"..score.."\n"
--		if score >= 0.8 * questionCount * 5 then
--			txt = txt.."You win! Congratulations!"
--		else 
--			txt = txt.."I'm sorry, you lost"
--		end
--		return txt
--	  end },
--	{ "explanation" },
--	{ "more explanation" },
--	{ label="end", "thanks for playing", ctype="special",
--		{ { blabel="new game", ctype="jump", skiplabel="start"},
--		  { blabel="exit", ctype="exit" } } }
--	}

function initFlow()
	Flow = {
	-- { ctype="jump", skiplabel="expla", "jump" },
	{ {"A game of trust","by Christiaan Janssen"}, ctype="next" },
    { "This game trusts you\nDo you trust the game?", ctype="next" },
	{ "Instructions" },
	{ function() return "You will be asked "..questionCount.." questions" end },
	{ "Each correct answer\nis worth 5 points" },
	{ function() return "You need to earn\nat least "..(questionCount*5*0.8).." points\nto win" end},
	{ "Don't lie" },
	{ 'You can quit any time by pressing esc\nand switch fullscreen with F12'},
	{ label="start", "Good luck", ctype="next", blabel="start",
	  pre = function() 
			score = 0 
			currentQuestionNumber = 1
	  end },

	{ ctype="question", "Say your name loud" },
	{ ctype="answer", "Did you say your name?" },
	
	{ ctype="question", 
		{ "How many eyes does a cat have?",
			"a) 1", "b) 2", "c) 4", "d) It's a trick question"} },
	{ ctype="answer", " b) a regular cat has 2 eyes"},

	{ ctype="question", pre = function() country_capitals = {
			{"France", "Paris"},
			{"Belgium", "Brussels"},
			{"Portugal", "Lisbon"},
			{"Netherlands", "Amsterdam"},
			{"United Kingdom", "London"},
			{"Germany", "Berlin"},
			{"Poland", "Warsaw"},
			{"Austria", "Vienna"},
			{"Bulgaria", "Sofia"},
			{"Croatia", "Zagreb"},
			{"Czech Republic", "Prague"},
			{"Denmark", "Copenhagen"},
			{"Sweden", "Stockholm"},
			{"Norway", "Oslo"},
			{"Finland", "Helsinki"},
			{"Greece", "Athens"},
			{"Hungary", "Budapest"},
			{"Ireland", "Dublin"},
			{"Italy", "Rome"},
			{"Romania", "Bucharest"},
			{"Russia", "Moscow"},
			{"Spain", "Madrid"},
			{"Switzerland", "Bern"},
			{"Ukraine", "Kiev"}
		} 
		local ndx = math.random(table.getn(country_capitals))
		tempvals = { country_capitals[ndx][1], country_capitals[ndx][2] } end,
		function() return "What is the capital of "..tempvals[1].." ?" end },
	{ ctype="answer", function() return "The capital of "..tempvals[1].."\nis "..tempvals[2].."\nDid you know it?" end },


	{ ctype="question", pre = function() tempvals = { math.random(1000)+100, math.random(1000)+100 } end,
	  function() return tempvals[1].." x "..tempvals[2].." = ?" end },
	{ ctype="answer", function() return tempvals[1].." x "..tempvals[2].." = "..(tempvals[1]*tempvals[2]).."\nDid you get it right?" end },
	{ "Whoa! That was not so easy!", ctype="next" },
	{ "Don't worry, not all questions will be that hard", ctype="next" },

	{ ctype="question", pre = function() local ndx = math.random(table.getn(country_capitals))
		tempvals = { country_capitals[ndx][1], country_capitals[ndx][2] } end,
		function() return "What is NOT the capital of "..tempvals[1].." ?" end },
	{ ctype="answer", function() return "There are many right answers\nBut "..tempvals[2].." would be a wrong answer\nDid you answer correctly?" end },

	{ label="here",ctype="question", "Berlin has a famous landmark.\nIt's a giant gate into the city.\nWhat is the name of this landmark?"},
	{ ctype="answer", "The name is 'Brandenburg gate'"},

	{ ctype="question", "Who wrote the greek tragedy 'Oedipus Rex' (Oedipus the king) ?"},
	{ ctype="answer", "Sophocles"},

	{ ctype="question", "Snowflake was the name of\nwhich animal from the zoo of Barcelona?"},
	{ ctype="answer", "Snowflake was an albino gorilla.\nDid you get it right?"},


	{ ctype="question", pre = function() local ndx = math.random(table.getn(country_capitals))
		tempvals = { country_capitals[ndx][1]:sub(1,1), country_capitals[ndx][1] } end,
		function() return "The name of which country starts with "..tempvals[1].." ?" end },
	{ ctype="answer", function() return "One example is "..tempvals[2].."\nCould you think of one?" end },

	{ "Let's take a break", ctype="next" },
	{ "If you need to,\nyou can go to the toilet now\nI won't move from here", ctype="jump", skiplabel="bucle1" },
	{ label="bucle1back", "Ok, let's breathe for a moment then", ctype="next" },
	{ label="bucle1", "Ready to continue?", blabels={"yes","no"}, skiplabel="bucle1back" },


	{ ctype="question", "Touch your right ear\nwith your left thumb\nAnd your nose\nwith your right thumb" },
	{ ctype="answer", "Could you do it?"},


	{ label="herea", ctype="question", "How long is the Chinese Great Wall?"},
	{ ctype="answer", "21,196 km (source: wikipedia).\nCorrect?"},


	{ ctype="question", "What is the chemical formula for salt?"},
	{ ctype="answer", "NaCl"},

	  
	{ ctype="question", "What is the name of the medical specialist who treats ear, nose and throat?"},
	{ ctype="answer", "otorhinolaryngologist"},


	{ ctype="question", "Lick your elbow"},
	{ ctype="answer", "Could you?"},

	 
	{ ctype="question", "Where are the Nazca Lines?"},
	{ ctype="answer", "Peru"},


	{ ctype="question", "How many bananas are\nin a pair of bananas?" },
	{ ctype="answer", "The answer is:\ntwo bananas\nDid you get it right?"},


	{ ctype="question", "Touch your toe with your right hand without bending your knees"},
	{ ctype="answer", "Did you do it?"},


	{ ctype="question", pre = function() tempval = string.char(string.byte("A") + math.random(24) - 1) end,
	    function() return "Do you have an "..tempval.." in your name?" end },
	{ ctype="answer", "I don't know the answer, but\ndid you?" },



-- { ctype="question", "(time: 2 minutes)
-- Go to the kitchen, get 2 glasses, and fill them with water.  You have to drink the water. You have 2 minutes.
-- "},
-- 	{ ctype="answer", "time over!
-- did you make it on time?
-- "},


	{ "That was the last question", ctype="next" },
	{ ctype = "next",
	  function() 
		local txt = "Final score:\n"..score.."\n"
		if score >= 0.8 * questionCount * 5 then
			txt = txt.."You win! Congratulations!"
		else 
			txt = txt.."I'm sorry, you lost\nYou needed "..(questionCount*5*0.8).." points"
		end
		return txt
	  end },
	{ "Some thoughts about this game"},
	{ "Games are often confrontations\nbetween the makers of the game\nand the players." },
	{ "The makers try to offer an experience\nthat is challenging\nas much as it is entertaining."},
	{ "The players try to overcome those challenges.\nThat is supposed to be the fun part."},
	{ "Some players try to outsmart the makers,\nand win the game without facing the challenges."},
	{ "Some makers dislike that.\nThey think that a player\nwho bends the rules to win\nis a bad player.\nThey call him a cheater."},
	{ "It's as if those makers think\n that they own the game.\nThe player is only a guest,\nwithout the right to play the way he wants."},
	{ "The games those makers build\ndo not trust the player.\nThe player has no reason\nto trust the game back"},
	{ "The whole experience becomes a confrontation\nwhere the player is being judged by the game,\nand sentenced guilty before starting."},
	{ "What if a game trusted the player?\nIf the player had the chance to play honestly\neven without a real prize\nnor a punishment at the end?"},
	{ "Just for the sake of playing honestly?"},
	{ "Did you play honestly?\nOnly you know."},
	{ label="end", "thanks for playing", ctype="special",
		{ { blabel="new game", ctype="jump", skiplabel="start"},
		  { blabel="exit", ctype="exit" } } }
	}
end	

