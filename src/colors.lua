function initColors()
	colors = {
	{254,252,138},
	{210,254,138},
	{149,179,244},
	{166,149,244},
	{244,149,159},
	{244,179,149}
	}

	colors.ndx = 1
end

function chooseColor()
	local newColor = colors.ndx
	while newColor == colors.ndx do
		newColor = math.random(table.getn(colors))
	end
	colors.ndx = newColor
	return newColor
end
