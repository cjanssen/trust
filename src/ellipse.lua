function love.graphics.ellipse(fillmode, x, y, w, h, n)
	if x == 0 or y == 0 then return end

	love.graphics.push()
	local r
	love.graphics.translate(x, y)
	if math.abs(h) > math.abs(w) then
		love.graphics.scale(1,h/w)
		r = w
	else
		love.graphics.scale(w/h,1)
		r = h
	end
	love.graphics.circle(fillmode, 0, 0, r, n)
	love.graphics.pop()
end
