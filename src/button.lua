
buttonColors = {
	chrome = {0,0,0},
	hover = {0,0,0,128},
	press = {0,0,0,192}
}

function newButton(_label, _x, _y, _callback)
	local _w = font:getWidth(_label)
	local _h = font:getHeight()

	local _ew = _w/2 + 20
	local _eh = _h/2 + 15

	return {
		label = _label,
		xfrac = _x,
		yfrac = _y,
		w = _w + 40,
		h = _h + 30,
		ew = _ew,
		eh = _eh,
		state = "normal",
		callback = _callback
	}
end

function drawButton(button)
	local ex = desiredSize[1] * button.xfrac
	local ey = desiredSize[2] * button.yfrac
	local x = ex - button.ew
	local y = ey - button.eh
	if button.state=="hover" then
		love.graphics.setColor(buttonColors.hover)
		love.graphics.ellipse("fill", ex, ey, button.ew, button.eh, 64)
	end
	if button.state=="press" then
		love.graphics.setColor(buttonColors.press)
		love.graphics.ellipse("fill", ex, ey, button.ew, button.eh, 64)
	end

	love.graphics.setColor(buttonColors.chrome)
	love.graphics.ellipse("line", ex, ey, button.ew, button.eh, 64)
	love.graphics.printf(button.label, x, y - font:getHeight()/2 + button.eh - 4, button.ew*2, "center")
end

function updateButton(button, dt)
	local ex = desiredSize[1] * button.xfrac
	local ey = desiredSize[2] * button.yfrac
	local x = ex - button.ew
	local y = ey - button.eh
	local mx = love.mouse.getX()/screenScale[1]
	local my = love.mouse.getY()/screenScale[2]
	if mx >= x and mx <= x + button.w and
		my >= y and my <= y + button.h then
		if love.mouse.isDown("l") then
			button.state = "press"
		else
			local callit = false
			if button.state == "press" then 
				callit = true
			end
			button.state = "hover"
			if callit then button.callback() end
		end
	else
		button.state = "normal"
	end
end

