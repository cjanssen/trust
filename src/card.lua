
function newCardFromFlow(cardndx)
	return newCardFromData(Flow[cardndx], cardndx)
end

function newCardFromData(cardnfo, cardndx)
	-- helper functions
	function showScore(txt)
		currentCard = newCardFromData({
			function()
				return txt.."\n"..score
			end, 
			ctype="next"}, 
			cardndx)
	end
	function findLabel(skiplabel)
		local skipto = cardndx+1		
		if skiplabel then
			for i,v in ipairs(Flow) do
				if v.label and v.label == skiplabel then
					skipto = i
					break
				end
			end
		else
			local n = table.getn(Flow)
			while not Flow[skipto].label and skipto <= n do
				skipto = skipto+1
			end
		end
		return skipto
	end

	-- preparation
	if cardnfo.pre then cardnfo.pre() end

	local ctype = cardnfo.ctype or "skip"
	local card = newCardBase(cardnfo[1])

	-- buttons
	if ctype=="next" then
		-- add "next" button
		local blabel = cardnfo.blabel or "next"
		card.buttons = {
			newButton(blabel, posit.hfwidth, posit.cincsis, function() advanceTo(cardndx+1) end)
		}
	elseif ctype=="skip" then
		-- add "skip" and "next" buttons
		local blabels = cardnfo.blabels or {"next", "skip"}

		-- find next label for skip
		local skipto = findLabel(cardnfo.skiplabel)

		-- generate buttons
		card.buttons = {
			newButton(blabels[1], posit.qtleft, posit.cincsis, function() advanceTo(cardndx+1) end),
			newButton(blabels[2], posit.qtright, posit.cincsis, function() advanceTo(skipto) end)
		}
	elseif ctype=="question" then
		-- q?
		local blabel = cardnfo.blabel or "done"
		card.buttons = {
			newButton(blabel, posit.hfwidth, posit.cincsis, function() advanceTo(cardndx+1) end)
		}

		-- card X of X
		local qcard = card
		card = newCardBase("Question "..currentQuestionNumber.." / "..questionCount)
		card.buttons = {
			newButton("ask",posit.hfwidth, posit.cincsis, function() 
				currentQuestionNumber = currentQuestionNumber + 1
				currentCard = qcard 
			end)
		}
	elseif ctype=="answer" then
		-- ANSWER:A correct?
		local cardscore = cardnfo.score or 5
		local blabels = cardnfo.blabels or {"yes","no"}
		card.buttons = {
			newButton(blabels[1], posit.qtleft, posit.cincsis, 
				function() 
					score = score + cardscore
					showScore("Correct!\nnew score:")
				end),
			newButton(blabels[2], posit.qtright, posit.cincsis, 
				function() 
					showScore("I am sorry\ncurrent score:")
				end)
			}

	elseif ctype=="jump" then
		local blabel = cardnfo.blabel or "next"
		local skipto = findLabel(cardnfo.skiplabel)
		card.buttons = {
			newButton(blabel, posit.hfwidth, posit.cincsis, function() advanceTo(skipto) end)
		}
	elseif ctype=="special" then
		local buttonNfo = cardnfo[2]
		local offsets = { posit.hfwidth }
		if table.getn(buttonNfo)==2 then
			offsets = { posit.qtleft, posit.qtright }
		end
		card.buttons = {}
		local blabels = {"next", "skip"}
		for i,v in ipairs(buttonNfo) do
			if v.ctype == "next" then
				cb = function() advanceTo(cardndx+1) end
			elseif v.ctype == "jump" or v.ctype == "skip" then
				local skipto = findLabel(v.skiplabel)
				cb = function() advanceTo(skipto) end
			elseif v.ctype == "exit" then
				cb = quitGame
			else
				cb = v.callback
			end
			table.insert(card.buttons,
				newButton(v.blabel or blabels[i], offsets[i], posit.cincsis, cb))
		end
	end
	return card
end


function newCardBase(_text)
	if type(_text) == "function" then
		_text = _text()
	end
	if type(_text) == "table" then
		_text = table.concat(_text, "\n")
	end
	local count = 1
	for i in _text:gmatch("\n") do count=count+1 end

	return {
		color = colors[chooseColor()],
		text = _text,
		height = count * font:getHeight(_text)
	}
end

function updateCard(card, dt)
	for i,v in ipairs(card.buttons) do
		updateButton(v)
	end
end

function drawCard(card)
	love.graphics.setColor(card.color)
	love.graphics.rectangle("fill",0,0,desiredSize[1], desiredSize[2])
	local y = desiredSize[2]/3 - card.height/2
	love.graphics.setColor(buttonColors.chrome)
	love.graphics.printf(card.text, 0, y, desiredSize[1], "center")

	for i,v in ipairs(card.buttons) do
		drawButton(v)
	end

end
