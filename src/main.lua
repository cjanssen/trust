

function include( filename )
	love.filesystem.load( filename )()
end

function love.load()
	include("ellipse.lua")
	include("colors.lua")
	include("button.lua")
	include("card.lua")
	include("flow.lua")
	include("cardlist.lua")

	-- global variables
	score = 0 
	currentQuestionNumber = 1
	currentCard = nil
	computePositioners()

	-- screen
	desiredSize = {1024, 768}
	screenScale = {1,1}
	initScreen(true)

	-- font
	font = love.graphics.newFont("DayPosterBlackNF.ttf", 36)
	love.graphics.setFont(font)

    -- initialize game
	initColors()
	initFlow()
	countQuestions()
	loadFirstCard()
end


function love.update(dt)
	updateCard(currentCard, dt)
end

function love.draw()
	love.graphics.push()
	love.graphics.scale(screenScale[1], screenScale[2])
	drawCard(currentCard)
	love.graphics.pop()
end

function quitGame()
	love.event.push("quit")
end

function love.keypressed(key)
	if key=="escape" then
		quitGame()
	end
	if key=="f12" then
		toggleFullscreen()
	end
end

function toggleFullscreen()
	local w,h,flags = love.window.getMode()
	initScreen(not flags.fullscreen)
end

function initScreen(fullscreen)

	local list = love.window.getFullscreenModes()
	if not list or table.getn(list) == 0 then return end
	local sz = { list[1].width, list[1].height }
	if fullscreen then
		screenScale = { sz[1] / 1024, sz[2] / 768 }
		local success = love.window.setMode( sz[1], sz[2], { fullscreen = true})
		if not success then initScreen(false) end
	else
		-- preferred size 1024x768, else 800x600 else 640x480
		if sz[1] < 800 or sz[2] < 600 then
			sz = { 640, 480 }
		elseif sz[1] < 1024 or sz[2] < 768 then
			sz = { 800, 600 }
		else
			sz = { 1024, 768 }
		end
		screenScale = { sz[1] / 1024, sz[2] / 768 }
		local success = love.window.setMode( sz[1], sz[2], { fullscreen = false})
	end
end

function computePositioners()
	posit = {
		qtleft = 1/4,
		qtright = 3/4,
		hfwidth = 1/2,
		cincsis = 5/6
	}
end




